import json
import os
import math
import numpy as np
from pitch_class_profiling import PitchClassProfiler




class DatasetPreprocess():

    def save_pitch(self):
        DATASET_PATH_0 = "datasets\\Julien Osmalsyj"
        DATASET_PATH_1 = "datasets\\Fabiana Vinci"
        JSON_PATH = "data.json"

        data = {
            "mapping": [],
            "labels": [],
            "pitch": []
        }

        i=-1
        for k, (dirpath, dirnames, filenames) in enumerate(os.walk(DATASET_PATH_0)):

            if dirpath is not DATASET_PATH_0:
                i+=1

                semantic_label = dirpath.split("\\")[-1]
                if semantic_label not in data["mapping"]:
                    data["mapping"].append(semantic_label)
                print("\nProcessing: {}".format(semantic_label))

                for f in filenames:

                    # load audio file
                    file_path = os.path.join(dirpath, f)

                    # process all segments of audio file

                    pitchClassProfiler=PitchClassProfiler(file_path)
                    data["pitch"].append(pitchClassProfiler.get_profile())
                    data["labels"].append(i)
        print("preprocessing Julien Osmalsyj successfuly executed")


        for k, (dirpath, dirnames, filenames) in enumerate(os.walk(DATASET_PATH_1)):
            if dirpath is not DATASET_PATH_1:
                tempLabelIndex = 0
                semantic_label = dirpath.split("\\")[-1]
                if semantic_label not in data["mapping"]:
                    data["mapping"].append(semantic_label)
                    i+=1
                    tempLabelIndex = i
                else:
                    tempLabelIndex = data["mapping"].index(semantic_label)
                print("\nProcessing: {}".format(semantic_label))

                for f in filenames:

                    # load audio file
                    file_path = os.path.join(dirpath, f)


                    pitchClassProfiler=PitchClassProfiler(file_path)
                    profile = pitchClassProfiler.get_profile()
                    if isinstance(profile[0], np.ndarray):
                        pitch = []
                        for rofl in profile:
                            pitch.append(rofl[0])
                        data["pitch"].append(pitch)
                    else:
                        data["pitch"].append(profile)
                    data["labels"].append(tempLabelIndex)
                    
        print("preprocessing Fabiana Vinci successfuly executed")

        # save pitch classes to json file
        with open(JSON_PATH, "w") as fp:
            json.dump(data, fp, indent=4)

